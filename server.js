const express = require('express');
const app = require('./api');

port = process.env.PORT || 3000;

app.use(express.static('web'));

app.listen(port, '0.0.0.0', () => console.log(`API service started on port ${port}..`));
